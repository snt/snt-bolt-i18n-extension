SNT i18n Extension
======================

Adds simple i18n to Bolt. Automatic forward of homepage to localized homepage, and twig function i18nPath($route, $params) for localized urls.

It also add a translate and currentLocale method.
