<?php

namespace Bolt\Extension\SNT\i18n;

use Bolt\Application;
use Bolt\BaseExtension;
use Bolt\Twig\Handler\RecordHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

/**
 * Class Extension
 * @author BobV
 */
class Extension extends BaseExtension
{

  /** @var array */
  protected $allowedLocales;

  /** @var string */
  protected $fallbackLocale;

  /**
   * Initializes the Twig extension
   */
  public function initialize() {
    // Retrieve allowed locales from config
    $this->allowedLocales = $this->config['allowed_locales'];

    if (isset($this->config['fallback'])) {
      // Use fallback from config
      $this->fallbackLocale = $this->config['fallback_locale'];
    } else {
      // Absolute fallback in code
      $this->fallbackLocale = 'en';
    }

    // Register Twig routing functions
    $this->addTwigFunction('sntPath', 'path');
    $this->addTwigFilter('sntCurrent', 'currentPath');

    // Register Twig translation functions
    $this->addTwigFunction('sntCurrentLocale', 'currentLocale');
    $this->addTwigFilter('sntTrans', 'trans');

    // Register before listener
    $this->app->before(array($this, 'beforeAction'));
  }

  /**
   * Listens to every request and sets the locale parameters accordingly
   *
   * @param Request     $request
   * @param Application $app
   *
   * @return null|\Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function beforeAction(Request $request, Application $app) {
    // Get the routing params
    $routeParams = $app['request']->get('_route_params');

    // Check if the locale is set
    if (!array_key_exists('_locale', $routeParams)) {
      // If not, determine the correct locale
      if ($request->hasSession()
          && $request->getSession()->get('_locale', '') != ''
          && in_array($request->getSession()->get('_locale', ''), $this->allowedLocales)
      ) {
        $preferredLocale = $request->getSession()->get('_locale');
      } else {
        $preferredLocale = $request->getPreferredLanguage($this->allowedLocales);
      }

      if ($preferredLocale === null) {
        $preferredLocale = $this->fallbackLocale;
      }

      // Save to session
      if ($request->hasSession()) {
        $request->getSession()->set('_locale', $preferredLocale);
      }

      // Forward to localized homepage if and only if the root document is requested
      if ($app['request']->getRequestUri() == '' || $app['request']->getRequestUri() == '/') {
        return $app->redirect($this->path('homepage', array('_locale' => $preferredLocale)));
      }
    } else {
      // If set, save to session if needed
      if ($request->hasSession()
          && $request->getSession()->get('_locale', '') != $routeParams['_locale']
          && in_array($routeParams['_locale'], $this->allowedLocales)
      ) {
        $request->getSession()->set('_locale', $routeParams['_locale']);
        return null;
      } else if (!in_array($routeParams['_locale'], $this->allowedLocales)) {
        // The supplied locale is not allowed, forward to fallback locale
        if ($request->hasSession()) {
          $request->getSession()->set('_locale', $this->fallbackLocale);
        }
        return $app->redirect($this->path('homepage', array('_locale' => $this->fallbackLocale)));
      }
    }

    return null;
  }

  /**
   * Creates the i18n url for the given path
   *
   * @param       $path
   * @param array $parameters
   *
   * @return string
   */
  public function path($path, $parameters = array()) {

    // Check empty path
    if ($path == '') {
      return '#';
    }

    // Parse options, check for _locale param
    if (!array_key_exists('_locale', $parameters)) {
      $parameters['_locale'] = $this->currentLocale();
    }

    // Generate the URL
    try {
      return $this->app->generatePath($path, $parameters);
    } catch (RouteNotFoundException $e){
      // Propably a direct path
      return '/' . $parameters['_locale'] . '/' . $path;
    }
  }

  /**
   * Translate a value
   *
   * @param       $content
   * @param array $params
   * @param null  $locale
   *
   * @return mixed
   */
  public function trans($content, $params = array(), $locale = NULL) {

    // Check locale
    if ($locale == NULL || !in_array($locale, $this->allowedLocales)) {
      $locale = $this->currentLocale();
    }

    // Find translation
    if (array_key_exists($content, $this->config['translations'])) {
      if (array_key_exists($locale, $this->config['translations'][$content])) {
        return strtr($this->config['translations'][$content][$locale], $params);
      }
    }

    return $content;

  }

  /**
   * Check if the given route is the current route
   *
   * @param $content
   *
   * @return bool
   */
  public function currentPath($content) {

    /** @var RecordHandler $recordHandler */
    $recordHandler = $this->app['twig.handlers']['record'];

    $i18nCurrent = $this->path($content);
    return $recordHandler->current($i18nCurrent);
  }

  /**
   * Get the current locale
   *
   * @return string
   */
  public function currentLocale() {

    // Retrieve locale parameters
    list($routeLocale, $requestLocale, $sessionLocale) = $this->getRouteParams();

    // If locale not set, retrieve it from one of the services
    if ($routeLocale != '' && in_array($routeLocale, $this->allowedLocales)) {
      return $routeLocale;
    } else if ($requestLocale != '' && in_array($requestLocale, $this->allowedLocales)) {
      return $requestLocale;
    } else if ($sessionLocale != '' && in_array($sessionLocale, $this->allowedLocales)) {
      return $sessionLocale;
    }

    // If not found, use fallback
    return $this->fallbackLocale;

  }

  /**
   * Get the extension name
   *
   * @return string
   */
  public function getName() {
    return "snt-i18n";
  }

  /**
   * Get the route params
   *
   * @return array
   */
  private function getRouteParams() {
    $routeParams = $this->app['request']->get('_route_params');
    if (array_key_exists('_locale', $routeParams)) {
      $routeLocale = $routeParams['_locale'];
    } else {
      $routeLocale = '';
    }
    $requestLocale = $this->app['request']->getLocale();
    $sessionLocale = $this->app['session']->get('_locale', '');
    return array($routeLocale, $requestLocale, $sessionLocale);
  }

}
